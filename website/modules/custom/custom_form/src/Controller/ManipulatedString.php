<?php

namespace Drupal\custom_form\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * @file
 * Contains \Drupal\custom_form\Form\ManipulatedString.
 */
/**
 * Show manipulated string form.
 */
class ManipulatedString extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function manipulationSubmit() {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $tempstore = \Drupal::service('user.private_tempstore')->get('custom_form');
    $result_data = $tempstore->get('string_data');

    return [
      '#attributes' => [
        'class' => 'string-manipulation',
        'id' => 'string-manipulation'
      ],
      '#theme' => 'string_manipulation',
      '#output_string' => $result_data['output_string'],
      '#input_string' => $result_data['input_string'],
      '#manipulation_type' => $result_data['manipulation_type']
    ];
  }


}