<?php

namespace Drupal\custom_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * @file
 * Contains \Drupal\custom_form\Form\ManipulatedString.
 */
/**
 * Manipulation Form.
 */
class StringManipulationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'string_manipulation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $manipulation_option = [
      'random' => t('Random'),
      'reverse' => t('Reverse'),
    ];
    $form['string_input'] = [
      '#type' => 'textfield',
      '#name' => 'string_input',
      '#title' => $this->t('String Input'),
      '#required' => TRUE
    ];
    $form['manipulation_type'] = [
      '#type' => 'select',
      '#name' => 'manipulation_type',
      '#title' => $this->t('Manipulation Type'),
      '#options' => $manipulation_option,
      '#required' => TRUE
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    //
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $input_string = $form_state->getValue(['string_input']);
    $selectStringSelectType = $form_state->getValue(['manipulation_type']);

    if($selectStringSelectType == 'reverse') { 
      $result = $this->reverseString($input_string, 1);
    }

    if($selectStringSelectType == 'random') { 
      $input_array = explode(' ', $input_string);
      shuffle($input_array);
      $result = implode(' ',$input_array);
    }

    $data = [
      'input_string' => $input_string,
      'output_string' => $result,
      'manipulation_type' => $selectStringSelectType
    ];

    $tempstore = \Drupal::service('user.private_tempstore')->get('custom_form');
    $tempstore->set('string_data', $data);
  
    $form_state->setRedirectUrl(Url::fromUri('internal:' . '/manipulated-string'));

  }

  /**
   * {@inheritdoc}
   */
  public function reverseString($string, $n){
    //explode the string by space
    $str_arr = explode(' ', $string);
   
    //get the n value
    $num = 1;
    $input_num = $n;
   
    $i = 0;
    $new_str = '';
    foreach ($str_arr as $str) {	
      $new_str = $new_str . ' ' . $str;
      $new_arr[$i] = $new_str;
   
      //if the $input_num is less than $num. reset $num to 1 (else cond.)
      if ($input_num > $num) {
        $num++;
      } 
      else {
        $i++;
        $num = 1;
        $new_str = '';
      }
    }
    
    //krsort to reverse the order of $new_arr
    krsort($new_arr);
    
    //now just implode the $new_arr again to form a sentence
    $final_str = implode(' ', $new_arr);
    return $final_str;
  }

}